
$(function() {

    $("#contactForm input,#contactForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {

        },
        submitSuccess: function($form, event) {
            event.preventDefault(); 
            var name = $("input#name").val();
            var email = $("input#email").val();
            var phone = $("input#phone").val();
            var paquete = $("select#paquete").val();
            var message = $("textarea#message").val();
            var firstName = name; 
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            $.ajax({
                url: "././mail/contact_me.php",
                type: "POST",
                data: {
                    name: name,
                    phone: phone,
                    email: email,
                    paquete: paquete,
                    message: message
                },
                cache: false,
                success: function() {
                  
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Tu mensaje a sido enviado!!. </strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                  
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append($("<strong>").text("Disculpa" + firstName + ", esto quiere decir que mi servidor no responde. Intentalo luego!"));
                    $('#success > .alert-danger').append('</div>');
                   
                    $('#contactForm').trigger("reset");
                },
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});



$('#name').focus(function() {
    $('#success').html('');
});
